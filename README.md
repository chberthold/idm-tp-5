# TP 5 - Parallélisme et bibliothèque CLHEP

## Installer la bibliothèque :

``` cd Random ```
``` ./configure --prefix=./ ```
``` make -j ```
``` make install ```

## Compiler le tp :

A la racine du TP :

``` g++ ./src/main.cpp -I./Random/include -L./Random/lib -c tp5 -lCLHEP-Random-2.1.0.0 -static ```

## Paramètres pour lancer l'exécutable :

1. Question 2 : archiver des fichiers de statuts

```./tp5 check``` pour générer les fichiers et vérifier la séquence générée

2. Question 3 : archiver dix statuts avec 2 milliards de tirages

```./tp5 save```

3. Question 4 : enchaîner 10 réplication de calcul de PI en séquentiel

```./tp5 pi sequential```

4. Question 5 : calculer PI en parallèle

```./getPiParallel.sh```