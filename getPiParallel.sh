#!/bin/bash

echo "Calculating PI..."

num_threads=5

declare -a results

calculate_average() {
    local total=0

    # Sum up all results
    for result in "${results[@]}"; do
        total=$(echo "$total + $result" | bc)
    done
    echo "scale=10; $total / $num_threads" | bc
}

start_time=$(date +%s.%N)

# Start threads with the right parameters
for ((i=0; i<$num_threads; i++)); do
  ./tp5 pi parallel ./fichiers/mt_status_2-$i > resultat_thread$i.txt &
done

wait

# Recover the results from temporary files
for ((i=0; i<$num_threads; i++)); do
    result=$(grep -oP '\d+\.\d+' resultat_thread$i.txt)
    results[$i]=$result
    rm resultat_thread$i.txt
done

end_time=$(date +%s.%N)

all_time=$(echo "$end_time - $start_time" | bc)

echo "Time taken : $all_time seconds"

average=$(calculate_average)

echo "Average for all threads : $average"

echo "Finished ! :)"