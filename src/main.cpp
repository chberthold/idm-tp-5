#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <string>
#include <sstream>
#include <iostream>
#include <assert.h>
#include <chrono>
#include <mutex>

#include "CLHEP/Random/MTwistEngine.h"
#define NB_STATUS 5
#define NB_TIRAGES 10

std::mutex mutex;

/*----------------------------------------*
 *  Concatenate a string and an integer   *
 *----------------------------------------*/
std::string operator+(const std::string& s, const int i){
  std::ostringstream stream;
  stream << s << i;
  return stream.str();
}

/*----------------------------------------*
 *               QUESTION 2               *
 *  Save NB_STATUS status separated by    *
 *  NB_TIRAGES numbers in                 *
 *  ../fichiers/mt_status_i and check if  *
 *  the status are reproductible.         *
 *----------------------------------------*
 */
void saveAndCheck(CLHEP::MTwistEngine * s){
  double saveTirages[NB_STATUS][NB_TIRAGES];
  double flat;

  std::cout << "Starting to save some status and checking the reproductibility..." << std::endl;

  // Save NB_STATUS files
  for(int status = 0; status < NB_STATUS; status++){
    std::cout << "Saving into mt_status_" << status << "..." << std::endl;
    s->saveStatus(operator+("./fichiers/mt_status_", status).c_str());

    // Get a small amount of numbers from the generator and save them
    for(int tirage = 0; tirage < NB_TIRAGES; tirage++){
      saveTirages[status][tirage] = s->flat();
    }
  }

  std::cout << std::endl;

  // Check if the status are good
  for(int status = 0; status < NB_STATUS; status++){
    std::cout << "Comparison for restore status " << status << " : " << std::endl;
    s->restoreStatus(operator+("./fichiers/mt_status_", status).c_str());

    for(int tirage = 0; tirage < NB_TIRAGES; tirage++){
      flat = s->flat();
      // Assert will fail the execution if the comparison is wrong
      assert(flat == saveTirages[status][tirage]);

      // Just for a visual checking
      std::cout << flat << " = " << saveTirages[status][tirage] << std::endl;
    }
  }
}


  /*----------------------------------------*
   *               QUESTION 3               *
   *  Save NB_STATUS status separated by    *
   *  2 000 000 000 numbers into            *
   *  ../fichiers/mt_status_2-i             *
   *----------------------------------------*
  */
void saveStatuts(CLHEP::MTwistEngine * s){
  std::cout << "Starting to save status separated by 2 000 000 000 numbers..." << std::endl;

  // Save the status in files
  for(int save = 0; save < NB_STATUS; save++){
    std::cout << "Saving into mt_status_2-" << save << "..." << std::endl;
    s->saveStatus(operator+("./fichiers/mt_status_2-", save).c_str());

    // Get a lot of numbers from the generator to separate the status
    for(long tirage = 0; tirage < 2'000'000'000; tirage++){
      s->flat();
    }
  }
}

  /*----------------------------------------*
   *             QUESTION 4/5               *
   *  Calculate PI with the Monte-Carlo     *
   *  method with nbPoints drawn            *
   *----------------------------------------*
  */
double calculatePI(CLHEP::MTwistEngine * s, int nbPoints){
  unsigned long pointsIn = 0;
  double point_x;
  double point_y;

  for(long point = 0; point < nbPoints; point ++){
    point_x = s->flat();
    point_y = s->flat();

    if(point_x * point_x + point_y * point_y <= 1){
      pointsIn ++;
    }
  }
  return 4 * (double)pointsIn / nbPoints;
}

  /*----------------------------------------*
   *               QUESTION 4               *
   *  Do NB_STATUS calculations of PI with  *
   *  the Monte-Carlo method. Done          *
   *  sequentially. Measure the time taken  *
   *----------------------------------------*
  */
void getPiSequential(CLHEP::MTwistEngine * s, long nbPoints){
  std::cout << "Starting to calculate PI sequentially..." << std::endl;
  double sum = 0;
  auto start = std::chrono::high_resolution_clock::now();

  for(int status = 0; status < NB_STATUS; status ++){
    s->restoreStatus(operator+("./fichiers/mt_status_2-", status).c_str());
    sum += calculatePI(s, nbPoints);
  }
  auto end = std::chrono::high_resolution_clock::now();
  auto time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  std::cout << "PI calculated : " << sum / NB_STATUS << " done in " << time.count() << " ms " << std::endl;
}

  /*----------------------------------------*
   *               QUESTION 5               *
   *  Do one calculation of PI with         *
   *  the Monte-Carlo method. Used for      *
   *  parallel calculations. Instanciate    *
   *  the generator with the status in the  *
   *  file provided in parameter            *
   *----------------------------------------*
  */
void getPiParallel(CLHEP::MTwistEngine * s, long nbPoints, const std::string & fileName){
  double pi;

  s->restoreStatus(fileName.c_str());
  pi = calculatePI(s, nbPoints);

  mutex.lock();
  std::cout << pi;
  mutex.unlock();
}

int main (int argc, char** argv)
{
  CLHEP::MTwistEngine * s = new CLHEP::MTwistEngine();
  if(argc > 1){
    std::string choice = std::string(argv[1]);

    if(choice == "check"){
      saveAndCheck(s);
    }

    else if(choice == "save"){
      saveStatuts(s);
    }

    else if(choice == "pi"){

      choice = std::string(argc > 2 ? std::string(argv[2]) : "");
      if(choice == "sequential"){
        getPiSequential(s, 1'000'000'000);
      }
      
      else if(choice == "parallel"){
        getPiParallel(s, 1'000'000'000, argv[3]);
      }
    }
  }

  delete s;

  return 0;
}

